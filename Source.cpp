#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <fstream>

using namespace std;

void initLogins(char*, char**, int&);
void copyString(char*, char**, int);
bool isCorrectLogin(char*, char**, int);
bool contains(char*, char**, int);
bool isLetter(const char);
bool isNumber(const char);
char toLowerCase(char);

int main() {
	const int N = 256;
	char** data = new char* [N];
	char fileName[N] = "logins.txt";
	int count = 0;

	initLogins(fileName, data, count);

	return 0;
}

void initLogins(char* fileName, char** data, int& count)
{
	const int N = 256;
	char* login = new char[N];
	ifstream streamOut;
	streamOut.open(fileName);
	
	if (!streamOut.is_open()) {
		cout << "\nCan't open file to read!";
		system("pause");
	}

	while (!streamOut.eof()) {
		streamOut.getline(login, N);

		bool isContain = isCorrectLogin(login, data, count);

		if (isContain) {
			cout << "YES" << endl;
			copyString(login, data, count);
			count++;
		}
		else {
			cout << "NO" << endl;
		}
	}
	
}

void copyString(char* source, char** array, int position)
{
	const int N = 256;
	char* word = new char[N];
	strcpy(word, source);
	array[position] = word;
}

bool isCorrectLogin(char* source, char** data, int count)
{
	int length = strlen(source);

	if (length < 2 || length > 24 || source[0] == '-' || contains(source, data, count)) {
		return false;
	}

	//bool occurrenceLetter = false;
	//bool occurrenceNumber = false;
	//bool occurrenceSymbol = false;

	for (int i = 0; i < length; i++)
	{
		char symbol = source[i];
		
		if (!isLetter(symbol) && !isNumber(symbol) && !(symbol == '-' || symbol == '_')) {
			return false;
		}
		/*if (isLetter(symbol)) {
			occurrenceLetter = true;
		}
		else if (isNumber(symbol)) {
			occurrenceNumber = true;
		}
		else if (symbol == '-' || symbol == '_') {
			occurrenceSymbol = true;
		}
		else {
			return false;
		}*/
	}

	return true;
	//if (occurrenceLetter && occurrenceNumber && occurrenceSymbol) {
	//	return true;
	//}
	//else {
	//	return false;
	//}
}

bool contains(char* source, char** data, int count)
{
	int length = strlen(source);

	for (int i = 0; i < count; i++)
	{
		if (length != strlen(data[i])) {
			continue;
		}

		bool flag = true;

		for (int j = 0; j < length; j++)
		{
			if (toLowerCase(source[j]) != toLowerCase(data[i][j])) {
				flag = false;
				break;
			}
		}

		if (flag) {
			return flag;
		}
	}

	return false;
}

bool isLetter(const char symbol)
{
	return (symbol >= 65 && symbol <= 90) || (symbol >= 97 && symbol <= 122);
}

bool isNumber(const char symbol)
{
	return (symbol >= 48 && symbol <= 57);
}

char toLowerCase(char symbol)
{
	if (isLetter(symbol) && (symbol >= 65 && symbol <= 90))
	{
		return symbol + 32;
	}

	return symbol;

}
